package test;


import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class SMTCServer2 {
    //Thread pool uop to ten working threads in one moment
    private static ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
    private static ServerSocket mainSocket;

    public SMTCServer2(ServerSocket socket){
        mainSocket = socket;
    }

    public void shutdownServer(){
        try {
            executorService.shutdownNow();
            mainSocket.close();
            System.exit(1);
        } catch (IOException e){
            e.getMessage();
            System.err.println("Something wrong was happen...");
        }
    }

    public static void main(String[] args) throws IOException {
        try{
            //checking if we have an argument for port number
            if(args.length != 1) {
                System.err.println("Incorrect arguments! Type TCP port number as only argument");
                System.exit(1);
            }
            int portNumber = Integer.parseInt(args[0]);
            ServerSocket mainSocket = new ServerSocket(portNumber);
            SMTCServer2 server = new SMTCServer2(mainSocket);

            while (true) {
                int queueSize = executorService.getQueue().size();
                Socket listeningSocket = mainSocket.accept();
                DataOutputStream responseToClient = new DataOutputStream(listeningSocket.getOutputStream());
                //limiting the queue of clients waiting for the thread by 10.
                //In total we will have 20: 10 working threads and 10 clients in the queue.
                //All other client will be discarded with message
                if(queueSize > 10) {
                    responseToClient.writeBytes("Server is overloaded... Sorry!\n");
                }else {
                    executorService.submit(new ServiceThread2(listeningSocket,server));
                }
            }
        }catch(Exception e){
            System.err.println(e);
            e.printStackTrace();
        }

    }
}
