package test;

import java.io.*;
import java.net.Socket;


public class ServiceThread2 implements Runnable {
    private Socket connectionSocket = null;
    private SMTCServer2 server;
    private boolean work = true;

    public ServiceThread2(Socket socket, SMTCServer2 server) {
        new Thread("ClientThread");
        this.connectionSocket = socket;
        //to be able to close the server from the thread we have reference on it in the thread
        this.server = server;
    }

    public void run() {
        try {
            //reading input from client
            BufferedReader messageFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            PrintWriter responseToClient = new PrintWriter(connectionSocket.getOutputStream(), true);

            while(work){
                // reading input line by line
                String sentence = messageFromClient.readLine();

                if (sentence.startsWith("HELO")) {
                    responseToClient.println(sentence + "\nIP:" + connectionSocket.getLocalAddress().toString().substring(1) + "\nPort: " + connectionSocket.getLocalPort() + "\nStudentID:13312345\n");
                } else if (sentence.equals("KILL_SERVICE")) {
                    work = false;
                    connectionSocket.close();
                    server.shutdownServer();
                    //for testing server
                    System.out.println("Received from client: " + sentence);
                } else {
                    //for testing server
                    System.out.println("Received from client: " + sentence);
                }
            }
        } catch (IOException e) {
            System.err.println("Can not listen to the socket:  " + connectionSocket.getLocalPort());
            e.getMessage();
            e.printStackTrace();
        }
    }
}